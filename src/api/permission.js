import request from "@/utils/request"

const api = {
  CheckLogin: '/isLogin',
  FetchAuthUrl: '/getSsoAuthUrl',
  TicketLogin: '/doLoginByTicket'
}

export function isLogin(){
  return request({
    url: api.CheckLogin,
    method: 'get'
  })
}

export function goSsoAuthUrl(clientUrl){
  return request({
    url: api.FetchAuthUrl,
    params: {clientLoginUrl: clientUrl}
  })
}

export function loginByTicket(ticket){
  return request({
    url: api.TicketLogin,
    params: {ticket}
  })
}
