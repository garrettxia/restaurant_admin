import {createRouter, createWebHistory} from "vue-router";
import { BasicLayout } from "@/layouts"

const routes = [
  {
    path: '/404',
    name: 'not-found',
    component: () => import('@/views/404.vue')
  },
  {
    path: "/",
    component: BasicLayout,
    redirect: '/dashboard',
    children: [
      {
        path: 'dashboard',
        component: () => import('@/views/home/index.vue'),
        name: "home",
        meta: { title: 'dashboard' }
      }
    ]
  },
  {
    path: "/user/login",
    name: "login",
    component: () => import('@/views/user/login.vue')
  },

  {
    path: '/:pathMatch(.*)*',
    redirect: '/404'
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router;
