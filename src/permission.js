import router from "./router";

import storage from 'store'
import { ACCESS_TOKEN } from '@/store/mutation-types'
import { isLogin } from "./api/permission";

const allowList = ['login', 'register', 'not-found']
const loginRoutePath = '/user/login'
const errorPagePath = '/404'

router.beforeEach((to,from,next) => {
  console.debug('路由鉴权...');
  if(storage.get(ACCESS_TOKEN)){
    next()
  }else{
    if (allowList.includes(to.name)){
      // 在免登录名单，直接进入
      next()
    }else{
      isLogin().then((res) => {
        if(res.status){
          // 获取token
          console.log('获取票据');
           next()
        }else{
          next({path: loginRoutePath, query: {back: location.href}})
        }
      }).catch(() => {
        next({path: errorPagePath})
      })
    }
  }
})
