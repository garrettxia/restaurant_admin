# 0.1.0 (2022-01-14)


### Bug Fixes

* **utils:** 网络请求工具 ([27a640f](https://gitee.com/garrettxia/restaurant_admin/commits/27a640ff6c32e6ace19c864377f9c45cab154182))


### BREAKING CHANGES

* **utils:** 废弃旧有的网络请求封装方式



## 0.0.1 (2022-01-14)


### Bug Fixes

* **complete init:** 完成项目初始化 ([e93e477](https://gitee.com/garrettxia/restaurant_admin/commits/e93e477b03f400664087e26fbc7019e4e9f7907d))
* **config:** 移动.cz-config.js到正确路径上 ([caa46bb](https://gitee.com/garrettxia/restaurant_admin/commits/caa46bbbba1fd8528fc53aa5d343fadbc379d7c0))
* **env:** 解决编辑器报错的问题 ([fd25600](https://gitee.com/garrettxia/restaurant_admin/commits/fd25600af02fea635869508ff9111c2344e8fe12))
* **exception:** error handler ([16499c3](https://gitee.com/garrettxia/restaurant_admin/commits/16499c3aa379cf7c4ee60d90734806fd8ec17ec3))
* **router:** 路由改变 ([4bd8663](https://gitee.com/garrettxia/restaurant_admin/commits/4bd866371a1b8ad9cc65de178b88edd2c0cc0e51))


### Features

* **鉴权:** 单点登录业务 ([91a2c57](https://gitee.com/garrettxia/restaurant_admin/commits/91a2c576a7dc3c7d2b88fa231d63bca3424c1b36))
* **axios:** 网络请求封装 ([022a101](https://gitee.com/garrettxia/restaurant_admin/commits/022a101e7d063a28fc80052c06a6aa70382a9346))
* **components:** 增加全局布局组件 ([4389b7e](https://gitee.com/garrettxia/restaurant_admin/commits/4389b7e7b9419b9d94804b2140ba398abfff08c9))
* **css&ui:** 样式组件 ([335c2fd](https://gitee.com/garrettxia/restaurant_admin/commits/335c2fd42784fae9e19d482cb4ccb0fc0e234dd4))
* **css:** antd ui ([fb467ea](https://gitee.com/garrettxia/restaurant_admin/commits/fb467ead77d23c6fa914f8fe7c036ed89e3d7b12))
* **element-ui:** 新增element-plus ui组件库 ([4fe1f08](https://gitee.com/garrettxia/restaurant_admin/commits/4fe1f088278f2f76ad89da7afc4d442366a79a14))
* **permission:** 增加鉴权 ([d493179](https://gitee.com/garrettxia/restaurant_admin/commits/d4931792fd7fa670f315f6e7a5d690a73746b3ef))
* **router:** 增加路由功能 ([4386c24](https://gitee.com/garrettxia/restaurant_admin/commits/4386c24c7ceb26361dc5bfd35886661f92e3135d))
* **router:** 重定向 ([90a7c8a](https://gitee.com/garrettxia/restaurant_admin/commits/90a7c8a104aab0784befb82f68d8cbc7943c9df5))
* **sso:** 单点登录 ([5497bb0](https://gitee.com/garrettxia/restaurant_admin/commits/5497bb0b097d2f489678aa2e818a4f0d6eaa45f0))
* **vuex:** 增加状态管理 ([5930607](https://gitee.com/garrettxia/restaurant_admin/commits/5930607745a6f6ae99631d281672ad6c357e103b))


### BREAKING CHANGES

* **element-ui:** 使得程序代码不用再手动编写import或者components 组件或依赖库名称
