import { defineConfig, loadEnv } from 'vite'
import createVitePlugins from './vite/plugins'

import path from 'path'

// https://vitejs.dev/config/
export default ({mode, command}) => {
  const env = loadEnv(mode, process.cwd())

  return defineConfig({
    plugins: createVitePlugins(env, command === 'build'),
    // 打包路径
    base: './',
    // 配置别名绝对路径  https://webpack.js.org/configuration/resolve/
    resolve: {
      alias: {
        // 如果报错__dirname找不到，需要安装node,执行npm install @types/node --save-dev
        "@": path.resolve(__dirname, "src")
      }
    },
    server: {
      // host: '0.0.0.0',
      port: 3000, // 服务端口号
      open: true //服务启动时是否自动打开浏览器
      // cors: true // 允许跨域
    },
    build: {
      terserOptions: {
        compress: {
          drop_console: true, // 生产环境去除console
          drop_debugger: true, // 生产环境去除debugger
        }
      }
    }
  })

}
