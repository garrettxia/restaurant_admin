import vue from '@vitejs/plugin-vue'

import createBanner from './banner'
import createRestart from './restart'
import createAutoImpot from './auto-import'
import createComponentsAutoImport from './components'

export default function createVitePlugins(viteEnv, isBuild = false) {
  const vitePlugins = [vue()]
  !isBuild && vitePlugins.push(createRestart())
  vitePlugins.push(createBanner())
  vitePlugins.push(createAutoImpot())
  vitePlugins.push(createComponentsAutoImport())
  return vitePlugins
}
