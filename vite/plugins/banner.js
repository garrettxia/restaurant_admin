import banner from 'vite-plugin-banner'

export default function createBanner() {
  return banner(`
  /**
   * name: Garrett Xia
   * homepage: https://gitee.com/garrettxia
   */
  `)
}
